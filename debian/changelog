pyorbital (1.9.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    - Refresh all patches.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Mon, 16 Dec 2024 17:25:08 +0000

pyorbital (1.9.1-2) unstable; urgency=medium

  * debian/patches:
    - New 0003-Reproducible-build.patch (Closes: #1089011).
  * Update d/copyright:
    - Update dates.
    - Drop unused license entries.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sat, 07 Dec 2024 10:40:12 +0000

pyorbital (1.9.1-1) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.7.0, no changes.

  [ Antonio Valentino ]
  * New upstream release.
  * debian/patches:
    - Refresh all patches.
  * debian/copyright:
    - Remove copyright entry from versioneer.
  * debian/control:
    - Add dependency on pybuild-plugin-pyproject,
      python3-hatchling, and python3-hatch-vcs.
    - Drop dependency on python3-setuptools.
  * debian/rules:
    - Skip broken test.
    - Fix permissions.
  * Update d/clean.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Tue, 03 Dec 2024 23:59:37 +0000

pyorbital (1.8.3-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    - Refresh all patches.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sat, 29 Jun 2024 10:56:46 +0000

pyorbital (1.8.2-2) unstable; urgency=medium

  * debian/patches:
    - New 0002-Fix-broken-test-on-i386.patch (Closes: #1068646).
  * debian/riles:
    - Skip broken tests.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sat, 13 Apr 2024 16:00:28 +0000

pyorbital (1.8.2-1) unstable; urgency=medium

  * New upstream release.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sat, 10 Feb 2024 09:03:41 +0000

pyorbital (1.8.1-1) unstable; urgency=medium

  * New upstream release.
  * Update dates in d/copyright.
  * debian/control:
    - Add build-dependency on python3-sphinx-rtd-theme and
      libjs-jquery-datatables.
  * debian/patches:
    - New 0001-Fix-pricacy-breach.patch.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sat, 06 Jan 2024 14:53:29 +0000

pyorbital (1.8.0-3) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Switch to dh-sequence-*.
  * Override dh_numpy3 to fix unused substitution variable.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sat, 09 Dec 2023 14:43:23 +0000

pyorbital (1.8.0-2) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Remove generated files in clean target.
    (closes: #1045918)
  * Use execute_after instead of override in rules file.

  [ Antonio Valentino ]
  * debian/control:
    - Use the <!nocheck> and <!nodoc> markers.
    - Update the package description.
    - Add build dependency on dh-sequence-numpy3.
  * Switch to Testsuite: autopkgtest-pkg-pybuild and remove
    the no longer needed d/tests folder.
  * debian/rules:
    - Drop unneeded target overrides.
  * Update doc build.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 13 Aug 2023 22:47:18 +0000

pyorbital (1.8.0-1) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Bump debhelper compat to 13.

  [ Antonio Valentino ]
  * New upstream release.
  * Update d/copyright.
  * debian/control:
    - add build dependency on pytest.
  * Fix autopkgtests.
  * debian/patches:
    - drop 0001-Fix-floating-point-comparison.patch,
      applied upstream.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Fri, 14 Jul 2023 08:13:29 +0000

pyorbital (1.7.3-2) unstable; urgency=medium

  [ Antonio Valentino ]
  * Improve d/python3-pyorbital.lintian-overrides formatting.
  * debian/patches:
    - new 0001-Fix-floating-point-comparison.patch (Closes: #1030492).
  * Update dates in d/copyright.

  [ Bas Couwenberg ]
  * Enable numpy3 dh helper.
  * Enable Salsa CI.
  * Bump Standards-Version to 4.6.2, no changes.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sat, 04 Feb 2023 08:51:37 +0000

pyorbital (1.7.3-1) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.6.1, no changes.
  * Use supported python3 versions in autopkgtest.

  [ Antonio Valentino ]
  * New upstream release.
  * Update lintian overrides.
  * Update d/copyright.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Tue, 12 Jul 2022 06:45:58 +0000

pyorbital (1.7.1-1) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.6.0, no changes.

  [ Antonio Valentino ]
  * New upstream release.
  * Update d/copyright file.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Thu, 23 Dec 2021 16:51:01 +0000

pyorbital (1.6.1-1) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Bump watch file version to 4.
  * Bump Standards-Version to 4.5.1, no changes.
  * Update watch file for GitHub URL changes.

  [ Antonio Valentino ]
  * New upstream release.
  * Update the debian/copyright file.
  * debian/patches:
    - drop 0001-Skip-tests-if-data-are-not-available.patch,
      applied upstream.
    - drop 0002-Fix-tests.patch, applied upstream.
  * debian/doc updated (README renamed into README.md).
  * Update debian/*.lintian-overrides

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Tue, 17 Aug 2021 15:21:52 +0000

pyorbital (1.6.0-3) unstable; urgency=medium

  [ Antonio Valentino ]
  * debian/tests/control:
    - update test dependencies (also depend on xarray and dask)
  * debian/patches:
    - drop 0001-Skip-tests-using-on-xarray-or-dask-if-they-are-not-a.patch,
      no longer necessary.
      Now the presence of xarray and dask is ensured during tests.
    - refresh and renumber all patches
    - new 0002-Fix-tests.patch, fix unittests on i386 platforms

  [ Bas Couwenberg ]
  * Add lintian override for embedded-javascript-library.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Mon, 20 Jul 2020 16:23:25 +0000

pyorbital (1.6.0-2) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * Add python3-requests to python3-pyorbital Depends for tlefile.

  [ Antonio Valentino ]
  * Add python3-requests to build dependencies

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 03 Jul 2020 17:15:20 +0200

pyorbital (1.6.0-1) unstable; urgency=medium

  * New upstream release.
  * Drop debian/compat file, and depend on debelper-compat.
  * Bump Standards-Version to 4.5.0, no changes.
  * Drop Name field from upstream metadata.
  * debian/patches:
    - refresh all patches
  * Update debian/copyright.
  * Replace use of deprecated $ADTTMP with $AUTOPKGTEST_TMP.
  * Add examples.
  * debian/control:
    - explicitly specify Rules-Requires-Root: no
  * Install script and manpage.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 28 Jun 2020 15:25:20 +0200

pyorbital (1.5.0-4) unstable; urgency=medium

  * Remove python2 from debian/tests

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Wed, 24 Jul 2019 05:27:41 +0000

pyorbital (1.5.0-3) unstable; urgency=medium

  [ Antonio Valentino ]
  * Bump Standards-Version to 4.4.0, no changes.
  * Update gbp.conf to use --source-only-changes by default.
  * Drop Python 2 support.
  * Set compat to 12.

  [ Bas Couwenberg ]
  * Drop ${shlibs:Depends} for arch: all packages.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 21 Jul 2019 14:18:08 +0000

pyorbital (1.5.0-2) unstable; urgency=medium

  * debian/patches
    - new 0002-Skip-tests-if-data-are-not-available.patch
      to skip tests that use data that are not installed.
      Fixex a debian ci issue.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 23 Dec 2018 07:31:41 +0000

pyorbital (1.5.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches
    - drop 0001-install-test-sub-package.patch: no longer necessary.
    - refresh and renumber remaining patches.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Thu, 20 Dec 2018 07:17:33 +0000

pyorbital (1.4.0-1) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Drop ancient X-Python{,3}-Version fields.
  * Bump Standards-Version to 4.2.1, no changes.
  * Update watch file to limit matches to archive path.
  * Replace tabs with spaces in control file.

  [ Antonio Valentino ]
  * New upstream release.
  * Update copyright file.
  * debian/control
    - add dependency from scipy
    - recommend dask and xarray
  * debian/rules
    - duild sphinx doc using Python 3
  * debian/patches
    - refresh all patches.
    - new 0002-Skip-tests-using-on-xarray-or-dask-if-they-are-not-a.patch.
      Skip tests using dask or xarray if they are not available.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 11 Nov 2018 10:21:51 +0000

pyorbital (1.3.1-1) unstable; urgency=medium

  [ Antonio Valentino ]
  * New upstream release.
  * Standard version bump to v4.1.4 (no change).
  * debian/patches
    - refresh all patches
  * debian/control
    - fix Homepage URL
  * Add upstream metadata.

  [ Bas Couwenberg ]
  * Update Vcs-* URLs for Salsa.
  * Fix name in upstream metadata.
  * Fix Source URL in copyright file.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Thu, 12 Apr 2018 06:20:26 +0000

pyorbital (1.2.0-1) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Add python3-sphinx to build dependencies.
  * Strip trailing whitespace from changelog.
  * Update copyright file, changes:
    - Group copyright holders by license
    - Strip .0 from GPL license shortname.

  [ Antonio Valentino ]
  * New upstream release
  * Standard version bumped to 4.1.3 (no change)
  * debian/control:
    - remove un-necessary Testsuite field
    - declare the *-doc package Multi-Arch foreign
  * Set compat to 11
  * debian/rules
    - remove unnecessary dh argument: --parallel

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 18 Feb 2018 09:58:31 +0000

pyorbital (1.1.1-1) unstable; urgency=medium

  * New upstream release

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Tue, 10 Jan 2017 19:48:45 +0000

pyorbital (1.1.0-2) unstable; urgency=medium

  [ Antonio Valentino ]
  * debian/control
    - fix Breaks and Replaces fields (Closes: #843352)

  [ Bas Couwenberg ]
  * Fix control file with cme.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 06 Nov 2016 09:00:39 +0000

pyorbital (1.1.0-1) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Team upload.
  * Override dh_python2 to call dh_numpy too.

  [ Antonio Valentino ]
  * New upstream release
  * Bump standard version to 3.9.8 (no change)
  * debian/copyright
    - fix the URL protocol of the Format field
  * New package for Python 3
  * New package for the HTMK documentation (previously included in
    the python-pyorbital package)

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 30 Oct 2016 09:04:12 +0000

pyorbital (1.0.1-1) unstable; urgency=medium

  [ Antonio Valentino ]
  * New upstream version
  * Remove 0002-relax-astronomy-test-condition.patch applied upstream
  * Switch to pybuild build system
  * Use dh --parallel in debian/rules

  [ Bas Couwenberg ]
  * Update watch file to handle common issues.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 21 Feb 2016 10:38:39 +0000

pyorbital (1.0.0-1) unstable; urgency=medium

  * New upstream release (Closes: #814071)
  * Fix debian/watch file
  * Refresh all patches
  * Bump standard version to 3.9.7 (no change)
  * debian/control
    - fix VCS URLs
  * debian/patches
    - new patch to relax test condition in test_astronomy,py

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Tue, 16 Feb 2016 20:05:23 +0000

pyorbital (0.3.2-1) unstable; urgency=low

  * Initial release (Closes: #752258)

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 22 Jun 2014 12:53:26 +0000
